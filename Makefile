all: cv

AUTHOR=FaroukM

FR_DIR=fr
EN_DIR=en

TARGET=main

# OS detected
ifeq ($(OS),Windows_NT)
		ifneq ($(findstring .exe,$(SHELL)),)
	OS_TYPE := Windows
		else
	OS_TYPE := Cygwin
		endif
else
	OS_TYPE := $(uname -s)
endif

TEX    = lualatex --shell-escape
RM     = $(if $(filter $(OS_TYPE),Windows),del /f /q ,rm -f )


# Routine
cv: fr_ver en_ver

fr_ver: $(FR_DIR)/$(TARGET).tex
	$(TEX) --output-directory=$(FR_DIR) --jobname=$(AUTHOR)_CV_FR $(TARGET).tex

en_ver: $(EN_DIR)/$(TARGET).tex
	$(TEX) --output-directory=$(EN_DIR) --jobname=$(AUTHOR)_CV_EN $(TARGET).tex

clean:
	-$(RM) $(FR_DIR)/*aux $(FR_DIR)/*log $(FR_DIR)/*out $(FR_DIR)/*fls \
							$(FR_DIR)/*fdb_latexmk $(FR_DIR)/*tex.bak \
							$(EN_DIR)/*aux $(EN_DIR)/*log $(EN_DIR)/*out \
							$(EN_DIR)/*fls $(EN_DIR)/*fdb_latexmk $(EN_DIR)/*tex.bak

	-@echo "all compilation tex -- except pdf -- files are deleted."


clean_all:
	-$(RM) $(FR_DIR)/*aux $(FR_DIR)/*log $(FR_DIR)/*out $(FR_DIR)/*fls \
							$(FR_DIR)/*fdb_latexmk $(FR_DIR)/*tex.bak $(FR_DIR)/*pdf \
							$(EN_DIR)/*aux $(EN_DIR)/*log $(EN_DIR)/*out $(EN_DIR)/*pdf \
							$(EN_DIR)/*fls $(EN_DIR)/*fdb_latexmk $(EN_DIR)/*tex.bak

	-@echo "all compilation tex files are deleted."



